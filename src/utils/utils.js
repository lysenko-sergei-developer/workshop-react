import bcrypt from 'bcrypt-nodejs'

export const encryptedPassword = (password) => bcrypt.hashSync(password)