import React, { Component } from 'react';

import Authentication from './components/Authentication'

import './App.css';

class App extends Component {
  constructor () {
    super()

    this.state = {
      authenticated: false
    }
  }

  handleChangeAuthenticated = () => this.setState ({ authenticated: !this.state.authenticated })

  render() {
    return (
      <div className="App">
        <Authentication />
      </div>
    )
  }
}

export default App;
