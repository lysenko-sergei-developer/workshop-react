import React, { Component } from 'react'
import { Form } from './form/Form'
import axios from 'axios' 

import { encryptedPassword } from '../utils/utils'

import './Authentication.css'

export default class Authentication extends Component {
  constructor () {
    super()

    this.state = {
      username: '',
      password: '',
      login: true,
      formError: ''
    }
}

  handleOnChangeUsername = (e) => this.setState ({ username: e.target.value })
  handleOnChangePassword = (e) => this.setState ({ password: e.target.value })

  handleChangeMode = () =>  {
    this.setState ({ login: !this.state.login })
    this.setState ({ formError: ''})
  }

  handleAuthentication = (token) => {
    localStorage.setItem('token', token)
  }
  
  receiveToken = () => {
    const url = 'https://grape-auth-boilerplate.herokuapp.com'
    const username = this.state.username
    const password = this.state.password

     axios.get(`${url}/users/token?username=${username}&password=${password}`)
      .then(response => this.handleAuthentication(response.data) )
      .catch(req => this.setState ({ formError: req.response.data.errors[0] }) )
  }

  createUser = () => {
    const url = 'https://grape-auth-boilerplate.herokuapp.com' // API

    const userData = {
      username: this.state.username,
      hashed_password: encryptedPassword(this.state.password)
    }

    console.log(userData)
    
    axios.post(`${url}/users`, userData)
      .then(res => res.status === 201 && this.receiveToken() )
      .catch(req => this.setState ({ formError: req.response.data}) )
  }

  handleCreateUser = (event) => {
    event.preventDefault()

    this.createUser()
  }

  handleReceiveToken = (event) => {
    event.preventDefault()

    this.receiveToken()
  }


  render () {

    return (
      <div>
        <ul className="tab-group">
           <li onClick={this.handleChangeMode} className={this.state.login ? 'tab active' : 'tab'}><a href="#login">Log In</a></li>
           <li onClick={this.handleChangeMode} className={!this.state.login ? 'tab active' : 'tab'}><a href="#signup">Sign Up</a></li>
         </ul>

        <Form paramId={'login'}
              login={this.state.login}
              username={this.state.username} 
              password={this.state.password}
              handleOnChangeUsername={this.handleOnChangeUsername}
              handleOnChangePassword={this.handleOnChangePassword}
              handleReceiveToken={this.handleReceiveToken}
        />

        <Form paramId={'sign-up'}
              login={!this.state.login}
              username={this.state.username} 
              password={this.state.password}
              handleOnChangeUsername={this.handleOnChangeUsername}
              handleOnChangePassword={this.handleOnChangePassword}
              handleCreateUser={this.createUser}
        />
      </div>
    )
  }
}